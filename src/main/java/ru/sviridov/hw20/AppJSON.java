package ru.sviridov.hw20;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

public class AppJSON {
    public static void main(String[] args) {
        String path = "src\\main\\java\\ru\\sviridov\\hw20\\";
        try {
            URL url = new URL("https://ghibliapi.herokuapp.com/films/2baf70d1-42bb-4437-b551-e5fed5a87abe");
            //https://ghibliapi.herokuapp.com/films
            try (InputStream is = url.openStream();) {
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
                objectMapper.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
                Film film = new Film();
                film = objectMapper.readValue(is, Film.class);
                objectMapper.writeValue(new File(path + "films.json"), film);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
