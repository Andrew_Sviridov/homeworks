package ru.sviridov.hw33;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MaxCountUniqCharsApp {
    public static void main(String[] args) {
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        Scanner in = new Scanner(System.in);
        in.useDelimiter("\r\n|\n");
        System.out.print("Input a String: ");
        String text = in.next();

        int temp = 0;
        char saveKey = 0;
        for (char c : text.toCharArray()) {
            if (Character.isLetter(c)) {
                if (map.containsKey(c)) {
                    map.put(c, map.get(c) + 1);
                    if (temp < map.get(c)) {
                        temp = map.get(c);
                        saveKey = c;
                    }
                } else {
                    map.put(c, 1);
                }
            }
        }
        if (map.containsKey(saveKey)) {
            System.out.println(saveKey + " " + map.get(saveKey));
        }
    }
}
