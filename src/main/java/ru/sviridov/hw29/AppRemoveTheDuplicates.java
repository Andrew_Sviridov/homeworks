package ru.sviridov.hw29;


import java.util.*;

public class AppRemoveTheDuplicates {
    public static void main(String[] args) {
        Map<String, Person> map = createMap();
        display(map);
        removeTheDuplicates(map);
        System.out.println();
        display(map);

    }

    public static Map<String, Person> createMap() {
        Map<String, Person> book = new HashMap<>();
        Person person1 = new Person(29, "Петрова", "жен");
        Person person2 = new Person(34, "Сидорова", "жен");
        Person person3 = new Person(34, "Тихонова", "жен");
        Person person4 = new Person(35, "Петров", "муж");
        book.put("Key1", person1);
        book.put("Key2", person1);
        book.put("Key3", person2);
        book.put("Key4", person3);
        book.put("Key5", person4);
        book.put("Key6", person4);
        return book;
    }

    public static void display(Map<String, Person> map) {
        for (Map.Entry<String, Person> item : map.entrySet()) {
            System.out.println(item.toString());
        }
    }

    public static void removeTheDuplicates(Map<String, Person> map) {
        Person person;
        int count = 0;
        String saveKey = "";
        //Collections.frequency()

        Set<Person> set = new HashSet<>(map.values());

        Iterator iter = set.iterator();
        while (iter.hasNext()) {
            person = (Person) iter.next();
            for (Map.Entry<String, Person> item : map.entrySet()) {
                if (person.equals(item.getValue())) {
                    count++;
                    if (count > 1) {
                        saveKey = item.getKey();
                    }
                }
            }
            if (count > 1) {
                removeItemFromMapByValue(map, person);
                map.put(saveKey, person);
            }
            count = 0;
        }
    }

    public static void removeItemFromMapByValue(Map<String, Person> map, Person value) {
        Map<String, Person> copy = new HashMap<>(map);
        for (Map.Entry<String, Person> pair : copy.entrySet()) {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }

}
