package ru.sviridov.hw29;

import java.util.Objects;

/**
 * Created by arty on 12.09.2018.
 */
public class Person implements Comparable<Person> {
    private String name;
    private int age;
    private String sex;

    public Person(int age, String name, String sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public String getSex() {
        return sex;
    }

    public int getAge() {
        return age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age &&
                name.equals(person.name) &&
                sex.equals(person.sex);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, sex);
    }

    public int compareTo(Person p) {
        int number = (age - p.getAge()) + name.compareTo(p.getName());
        if (number == 0) {
            return 0;
        } else if (number > 0) {
            return 1;
        } else {
            return -1;
        }

    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                '}';
    }
}

