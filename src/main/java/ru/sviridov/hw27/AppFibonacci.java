package ru.sviridov.hw27;

public class AppFibonacci {
    public static void main(String[] args) {
        for (int i = 1; i < 10; i++) {
            System.out.print(fib(i) + " ");
        }
        System.out.println();
        for (int i = 1; i < 10; i++) {
            System.out.print(fib2(i) + " ");
        }
        System.out.println();

        System.out.println(fibStr2(8));
        System.out.println(fibStr(8));
    }

    public static long fib(long n) {
        if (n < 3) {
            return 1;
        } else {
            return fib(n - 1) + fib(n - 2);
        }
    }

    public static String fibStr(int n) {
        String result = "";
        for (int i = 1; i <= n; i++) {
            result = result + fib(i) + " ";
        }
        return result;
    }

    public static long fib2(long n) {
        if (n < 3) {
            return 1;
        } else {
            long number1 = 1;
            long number2 = 1;
            long result = 0;

            for (int i = 3; i <= n; i++) {
                result = number1 + number2;
                number1 = number2;
                number2 = result;
            }
            return result;
        }

    }

    public static String fibStr2(long n) {
        if (n < 3) {
            return "1 1";
        } else {
            long number1 = 1;
            long number2 = 1;
            long result = 0;
            String str = "1 1 ";
            for (int i = 3; i <= n; i++) {
                result = number1 + number2;
                str = str + result + " ";
                number1 = number2;
                number2 = result;
            }
            return str;
        }

    }

}
