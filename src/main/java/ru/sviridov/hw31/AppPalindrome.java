package ru.sviridov.hw31;

import java.util.Scanner;

public class AppPalindrome {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Input a String: ");
        String text = in.next();
        System.out.println(func1(text));
        System.out.println(func2(text));
    }

    public static boolean func1(String text) {
        StringBuilder str = new StringBuilder(text);
        str.reverse();

        return text.equals(str) ? true : false;
    }

    public static boolean func2(String text) {
        char arr[] = text.toCharArray();
        for (int j = 0; j < (text.length() - 1) / 2; j++) {
            if (arr[j] != arr[text.length() - 1 - j]) {
                return false;
            }
        }
        return true;
    }

}
