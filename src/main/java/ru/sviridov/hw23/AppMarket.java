package ru.sviridov.hw23;

public class AppMarket {
    public static void main(String[] args) {
        MyBasket basket = new MyBasket();

        basket.addProduct("Яблоко", 2);
        basket.addProduct("Банан", 3);
        basket.addProduct("Орех", 1);
        basket.addProduct("Груша", 2);


        System.out.println(basket.getProducts());
        basket.removeProduct("Банан");
        System.out.println(basket.getProducts());
        System.out.println(basket.getProductQuantity("Банан"));

        basket.updateProductQuantity("Банан", 8);

        System.out.println(basket.getProducts());
        System.out.println(basket.getProductQuantity("Банан"));

        basket.clear();
        System.out.println(basket.getProducts());

    }
}
