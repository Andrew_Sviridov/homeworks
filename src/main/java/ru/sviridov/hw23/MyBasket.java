package ru.sviridov.hw23;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MyBasket implements Basket {
    private ArrayList<String> list = new ArrayList<>();

    public void addProduct(String product, int quantity) {
        for (int i = 0; i < quantity; i++) {
            list.add(product);
        }

    }

    public void removeProduct(String product) {
        list.remove(product);
    }

    public void updateProductQuantity(String product, int quantity) {
        ArrayList<String> listUpdateProduct = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            listUpdateProduct.add(product);
        }

        int number = getProductQuantity(product) - listUpdateProduct.size();
        if (number <= 0) {
            addProduct(product, Math.abs(number));
        } else {
            for (int i = 0; i < number; i++) {
                removeProduct(product);
            }
        }

    }

    public void clear() {
        list.clear();
    }

    public List<String> getProducts() {
        return list;
    }

    public int getProductQuantity(String product) {
        Iterator iter = list.iterator();
        int result = 0;
        while (iter.hasNext()) {
            if (iter.next().equals(product)) {
                result++;
            }
        }
        return result;
    }

}
