package ru.sviridov.hw9;

public class Dog extends Animal implements Run, Swim {
    String name = "Собака";

    public Dog(String name) {
        super(name);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void run() {

    }

    @Override
    public void swim() {

    }
}
