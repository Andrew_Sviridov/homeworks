package ru.sviridov.hw9;

public class Cat extends Animal implements Swim, Run {
    String name = "Кот";

    public Cat(String name) {
        super(name);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void run() {

    }

    @Override
    public void swim() {

    }
}
