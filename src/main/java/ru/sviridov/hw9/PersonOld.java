package ru.sviridov.hw9;

public class PersonOld extends Human {
    protected String name;
    protected String lastName;
    protected int numberPhone;

    public PersonOld(String name, String lastName, int numberPhone) {
        this.name = name;
        this.lastName = lastName;
        this.numberPhone = numberPhone;
    }

    @Override
    public void run() {
        System.out.println("бегает медленно");
    }

    @Override
    public void swim() {
        System.out.println("плавает медленно");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(int numberPhone) {
        this.numberPhone = numberPhone;
    }
}
