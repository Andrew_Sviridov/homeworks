package ru.sviridov.hw25;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class AppIsUnique {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("Вася", "Иванов");
        map.put("Петр", "Петров");
        map.put("Виктор", "Иванов");
        map.put("Сергей", "Савельев");
        map.put("Вадим", "Петров");
        System.out.println(map);
        System.out.println(isUnique(map));

    }

    public static boolean isUnique(Map<String, String> map) {
        if (map.isEmpty())
            return true;
        String temp = "";
        int count = 0;
        for (Entry<String, String> entry : map.entrySet()) {
            if (temp.equals(entry.getValue())) {
                System.out.println("1: " + entry.getKey() + "; 2: " + entry.getValue());
                count++;
            }
            temp = entry.getValue();
        }
        if (count >= 2) {
            return true;
        }
        return false;
    }

}
