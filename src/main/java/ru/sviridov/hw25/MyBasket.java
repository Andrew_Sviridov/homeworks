package ru.sviridov.hw25;

import java.util.*;

public class MyBasket implements Basket {
    private Map<String, Integer> map = new HashMap<>();

    public void addProduct(String product, int quantity) {
        map.put(product, quantity);
    }

    public void removeProduct(String product) {
        map.remove(product);
    }

    public void updateProductQuantity(String product, int quantity) {
        map.put(product, quantity);
    }

    public void clear() {
        map.clear();
    }


    public ArrayList<String> getProducts() {
        ArrayList<String> list = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            list.add(entry.getKey() + " - " + entry.getValue());
        }
        return list;
    }


    public int getProductQuantity(String product) {
        return map.get(product) == null ? 0 : map.get(product);
    }

}
