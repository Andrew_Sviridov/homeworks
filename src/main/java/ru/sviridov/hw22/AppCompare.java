package ru.sviridov.hw22;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AppCompare {
    public static void main(String[] args) {
        List<Person> people = new ArrayList<>();
        people.add(new Person("Tom", 15));
        people.add(new Person("Nick", 22));
        people.add(new Person("Alice", 80));
        people.add(new Person("Bill", 6));


        System.out.println("People list");
        System.out.println();

        for (Person person : people) {
            System.out.println(person.getName() + " " + person.getAge());
        }
        System.out.println("----------------------------------------------------");

        //Collections.sort(people);
        Collections.sort(people, new PersonSuperComparator());

        for (Person person : people) {
            System.out.println(person.getName() + " " + person.getAge());
        }
    }
}
