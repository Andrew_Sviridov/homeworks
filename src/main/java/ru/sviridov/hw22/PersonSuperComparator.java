package ru.sviridov.hw22;

import java.util.Comparator;

public class PersonSuperComparator implements Comparator<Person> {
    public int compare(Person a, Person b) {
        return a.compareTo(b);
    }
}
