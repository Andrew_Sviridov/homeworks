package ru.sviridov.hw22;

/**
 * Created by arty on 12.09.2018.
 */
public class Person implements Comparable<Person> {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int compareTo(Person p) {
        int number = (age - p.getAge()) + name.compareTo(p.getName());
        if (number == 0) {
            return 0;
        } else if (number > 0) {
            return 1;
        } else {
            return -1;
        }

    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}

