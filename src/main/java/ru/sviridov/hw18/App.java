package ru.sviridov.hw18;

import java.io.*;

public class App {
    public static void main(String[] args) {
        String path = "src\\main\\java\\ru\\sviridov\\hw18\\";

        String imputString = "Привет, мир!";
        saveToFile(path + "windows-1251.txt", imputString, "windows-1251");

        convert(path + "windows-1251.txt", path + "copyFileToUTF8.txt", "windows-1251", "UTF8");
    }

    private static void saveToFile(String outFile, String imputString, String charsetName) {
        try (OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(outFile), charsetName)) {
            osw.write(imputString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void convert(String inFile, String outFile, String inEncoding, String outEncoding) {
        saveToFile(outFile, readFromFile(inFile, inEncoding), outEncoding);
    }

    private static String readFromFile(String filename, String encoding) {
        String result = "";
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(filename), encoding))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                result += line;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
