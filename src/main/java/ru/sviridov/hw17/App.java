package ru.sviridov.hw17;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        //init
        ArrayList<Book> books = new ArrayList<Book>();
        ArrayList<Book> books2 = new ArrayList<Book>();
        books.add(new Book("111", "2222", new Date(1990, 5, 19)));
        books.add(new Book("333", "4444", new Date(1991, 6, 20)));
        books.add(new Book("555", "667", new Date(1992, 7, 21)));

        ManagerData.saveBooks(books);

        //-----------------------------------------------------------

        Scanner scan = new Scanner(System.in);
        printHelp();

        try {
            ManagerData.fileStatus();
            while (scan.hasNext()) {
                String command = scan.next();
                switch (command) {
                    case "add": {
                        addBook(scan);
                        break;
                    }
                    case "getBooks": {

                        for (Book book : ManagerData.loadBooks()) {
                            System.out.println(book.toString());
                        }
                        break;
                    }
                    case "end": {

                        return;
                    }
                    default:
                        System.out.println("Команда не определена");
                }
                scan.nextLine();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void addBook(Scanner scan) {
        try {
            Book book = new Book();
            System.out.println("Введите название книги");
            book.setTitle(scan.next());
            System.out.println("Введите автора книги");
            book.setAuthor(scan.next());
            System.out.println("Введите дату(dd.MM.yyyy) публикации книги");
            String dateFormat = "dd.MM.yyyy";

            book.setDatePublication(new SimpleDateFormat(dateFormat).parse(scan.next()));
            ManagerData.saveBook(book);
            System.out.println("добавили книгу: " + book.toString());
        } catch (Exception e) {
            System.out.println("ошибка ввода данных. книга не добавлена");
        }

    }

    /**
     * выводит подсказку по доступным командам
     */
    private static void printHelp() {
        System.out.println("Введите 'add' для добавления книги");
        System.out.println("Введите 'getBooks' для получения списка книг");
        System.out.println("Введите 'end' для выхода");
    }


}
