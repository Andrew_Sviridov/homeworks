package ru.sviridov.hw17;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;

import static javafx.application.Platform.exit;

public class ManagerData {
    static String fileName = "src\\main\\java\\ru\\sviridov\\hw17\\Library.bin";
    static File file1 = new File(fileName);

    public static void fileStatus() throws IOException {
        if (file1 != null & file1.exists() & file1.isFile()) {
            System.out.println("файл библеотеки существует");
        } else {
            System.out.println("файл библеотеки не существует");
            System.exit(0);
        }
    }

    public static void saveBook(Book book) {
        ArrayList<Book> books = new ArrayList<Book>();
        books = loadBooks();
        books.add(book);
        try (
                ObjectOutputStream s = new ObjectOutputStream(new FileOutputStream(fileName));

        ) {

            s.writeObject(books);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void saveBooks(ArrayList<Book> books) {
        try (
                ObjectOutputStream s = new ObjectOutputStream(new FileOutputStream(fileName));
        ) {
            s.writeObject(books);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static ArrayList<Book> loadBooks() {
        ArrayList<Book> books = new ArrayList<Book>();
        try (
                ObjectInputStream s = new ObjectInputStream(new FileInputStream(fileName));
        ) {
            books = ((ArrayList<Book>) s.readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return books;
    }
}
