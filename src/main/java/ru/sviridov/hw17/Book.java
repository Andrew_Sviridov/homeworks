package ru.sviridov.hw17;

import java.io.Serializable;
import java.util.Date;

public class Book implements Serializable {
    final static long serialVersionUID = 1l;

    private String title;
    private String author;
    private Date datePublication;

    public Book(String title, String author, Date datePublication) {
        this.title = title;
        this.author = author;
        this.datePublication = datePublication;
    }

    public Book() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;
    }


    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", datePublished=" + datePublication +
                '}';
    }
}
