package ru.sviridov.hw8.myClass;

public class Simple {

    private Simple() {
    }

    //
    public static class Bilder {
        static int count = 0;
        Simple myclass;

        public Bilder() {
            this.myclass = new Simple();
            count++;
        }

        public static int getCount() {
            return count;
        }

        public Simple getMyclass() {
            return myclass;
        }
    }
}
