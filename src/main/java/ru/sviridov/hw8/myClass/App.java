package ru.sviridov.hw8.myClass;

public class App {
    public static void main(String[] args) {
        Simple testClass = new Simple.Bilder().getMyclass();
        System.out.println(Simple.Bilder.getCount() + "          1 --- " + testClass.getClass());
        Simple testClass2 = new Simple.Bilder().getMyclass();
        Simple testClass3 = new Simple.Bilder().getMyclass();
        Simple testClass4 = new Simple.Bilder().getMyclass();
        System.out.println(Simple.Bilder.getCount() + "          2 --- " + testClass4.getClass());
    }
}
