package ru.sviridov.hw8.conv;

import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

public class Contract {
    int number;
    Date date;
    String listGoods[];

    public Contract(int number, Date date, String[] listGoods) {
        this.number = number;
        this.date = date;
        this.listGoods = listGoods;
    }

}
