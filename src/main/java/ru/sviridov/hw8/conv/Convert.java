package ru.sviridov.hw8.conv;

public class Convert {
    public static Act convert(Contract classContract) {
        return new Act(classContract.number, classContract.date, classContract.listGoods);
    }
}
