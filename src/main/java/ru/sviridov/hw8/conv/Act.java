package ru.sviridov.hw8.conv;

import java.util.Date;

public class Act {
    int number;
    Date date;
    String listGoods[];

    public Act(int number, Date date, String[] listGoods) {
        this.number = number;
        this.date = date;
        this.listGoods = listGoods;
    }
}
