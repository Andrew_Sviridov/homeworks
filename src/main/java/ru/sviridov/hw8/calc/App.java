package ru.sviridov.hw8.calc;


import static ru.sviridov.hw8.calc.Calculator.*;

public class App {

    public static void main(String[] args) {

        Object num1 = get();
        Object num2 = get();
        char operation = getOperation();
        if (num1.getClass() == Integer.class & num2.getClass() == Integer.class) {
            System.out.println("Результат операции: " + calc((int) num1, (int) num2, operation));
        } else if (num1.getClass() == Double.class & num2.getClass() == Integer.class) {
            System.out.println("Результат операции: " + calc((double) num1, (int) num2, operation));
        } else if (num1.getClass() == Integer.class & num2.getClass() == Double.class) {
            System.out.println("Результат операции: " + calc((int) num1, (double) num2, operation));
        } else {
            System.out.println("Результат операции: " + calc((double) num1, (double) num2, operation));
        }

    }
}
