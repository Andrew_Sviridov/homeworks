package ru.sviridov.hw7;

public enum Drinks {
    COKE("КОКАКОЛА", 10),
    LEMONADE("ЛЕМОНАД", 50),
    COCOA("КОКАО", 15),
    WATER("ВОДА", 30),
    COFFEE("КОФЕ", 58),
    TEA("ЧАЙ", 22);

    private String title;
    private int coast;

    Drinks(String title, int coast) {
        this.title = title;
        this.coast = coast;
    }

    public String getTitle() {
        return title;
    }

    public int getCoast() {
        return coast;
    }

}
