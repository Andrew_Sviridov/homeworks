package ru.sviridov.hw7;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {

        int account = 0;
        int numberIn = 0;
        boolean flagLoop = true;
        Drinks[] drinks = Drinks.values();

        System.out.println("Вендинговый автомат");

        Scanner in = new Scanner(System.in);

        while (flagLoop) {
            System.out.println("Выберите действие. Чтобы выбрать введите номер действия.:");
            for (String i : new String[]{" 1 - Просмотреть список", " 2 - Внести на счет", " 3 - Выбрать номер напитка", " 4 - Выход"}) {
                System.out.println(i);
            }
            numberIn = 0;
            numberIn = in.nextInt();
            switch (numberIn) {
                case (1):
                    System.out.println("Ваш счет: " + account);
                    System.out.printf("%-5s%-11s%-10s%n", "№", "Название", "Цена");
                    System.out.println("----------------------");

                    for (int j = 0; j < drinks.length; j++) {
                        System.out.printf("%-5d%-11s%-10d%n", (j + 1), drinks[j].getTitle(), drinks[j].getCoast());

                        // System.out.println("№ " + (j + 1) + " " + drinks[j].getTitle() + "\t цена: " + drinks[j].getCoast());
                    }
                    break;
                case (2):
                    System.out.println("Ваш счет: " + account);
                    System.out.println("Введите сумму которую добавить на счет");
                    account += in.nextInt();
                    break;
                case (3):
                    if (account != 0) {
                        System.out.println("Ваш счет: " + account);
                        System.out.println("Введите номер продукта");
                        int numberProduct = in.nextInt() - 1;
                        if (numberProduct <= drinks.length & 0 <= numberProduct) {
                            if (drinks[numberProduct].getCoast() <= account) {
                                account -= drinks[numberProduct].getCoast();
                                System.out.println("Купили " + drinks[numberProduct].getTitle());
                                System.out.println("Ваш счет: " + account);
                                break;
                            } else {
                                System.out.println("Недостаточно стредств на счете");
                                break;
                            }
                        } else {
                            System.out.println("Нет продукта с таким номером");
                            break;
                        }
                    } else {
                        System.out.println("Недостаточно стредств на счете");
                    }
                    break;
                case (4):
                    flagLoop = false;
                    break;
            }

        }
        in.close();
    }
}