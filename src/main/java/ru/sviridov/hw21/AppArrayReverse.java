package ru.sviridov.hw21;

public class AppArrayReverse {
    static int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    public static void main(String[] args) {
        display();
        Reverse();
        display();
    }

    private static void display() {
        for (int j = 0; j < array.length; j++)
            System.out.print(array[j] + " ");
        System.out.println("");
    }

    private static void Reverse() {
        for (int h = 0; h < array.length / 2; h++) {
            Swap(h, array.length - 1 - h);
        }
    }

    private static void Swap(int one, int two) {
        int temp = array[one];
        array[one] = array[two];
        array[two] = temp;
    }
}
