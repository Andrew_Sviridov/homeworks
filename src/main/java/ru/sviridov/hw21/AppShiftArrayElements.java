package ru.sviridov.hw21;

import java.util.Random;

public class AppShiftArrayElements {
    static int[][] array = new int[10][10];

    public static void main(String[] args) {
        createArray();
        System.out.println();
        toLeft();
    }

    public static void toLeft() {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                array[i][j] = array[i][j + 1];
                System.out.print("\t" + array[i][j]);
            }
            array[i][array.length - 1] = 0;
            System.out.print("\t" + array[i][array.length - 1]);
            System.out.println();
        }

    }

    private static void createArray() {
        Random rnd = new Random();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                array[i][j] = rnd.nextInt(100);
                System.out.print("\t" + array[i][j]);
            }
            System.out.println();
        }
    }


}
