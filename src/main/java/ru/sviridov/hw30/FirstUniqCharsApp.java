package ru.sviridov.hw30;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class FirstUniqCharsApp {
    public static void main(String[] args) {
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        //Scanner in = new Scanner(System.in);
        //System.out.print("Input a String: ");
        //String text = in.next();
        String text = "total";
        for (char c : text.toCharArray()) {
            if (map.containsKey(c)) {
                map.put(c, map.get(c) + 1);
            } else {
                map.put(c, 1);
            }

        }

        for (char c : text.toCharArray()) {
            if (map.get(c) == 1) {
                System.out.println(c);
                break;
            }
        }
    }
}
