package ru.sviridov.hw24;

import java.util.HashSet;
import java.util.Set;

public class AppRemoveFromSet {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("foo");
        set.add("buzz");
        set.add("bar");
        set.add("");
        set.add(null);
        System.out.println(" origianlSet " + set);

        System.out.println(" copySet     " + removeEventLenght(set));

    }

    public static Set<String> removeEventLenght(Set<String> set) {
        Set<String> set2 = new HashSet<>();
        set2.addAll(set);
        set2.removeIf(j -> {
            if (j != null) {
                //System.out.println(j+" "+j.length());
                return j.length() % 2 == 0;
            }
            return false;
        });
        return set2;
    }
}
