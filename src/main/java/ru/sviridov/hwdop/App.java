package ru.sviridov.hwdop;

import java.util.Arrays;

class App {
    public static void main(String[] args) {
        SortClass MyClass = new SortClass(5);
        System.out.println("Было");
        System.out.println(Arrays.toString(MyClass.getArrayInt()));

        MyClass.bubleSort(MyClass.getArrayInt());

        System.out.println("Стало");
        System.out.println(Arrays.toString(MyClass.getArrayInt()));

        //---------------------------------------------------------

        SortClass MyClass2 = new SortClass(5);
        System.out.println("Было");
        System.out.println(Arrays.toString(MyClass2.getArrayInt()));

        int low = 0;
        int high = MyClass2.getArrayInt().length - 1;

        MyClass.quickSort(MyClass2.getArrayInt(), low, high);

        System.out.println("Стало");
        System.out.println(Arrays.toString(MyClass2.getArrayInt()));

    }
}
