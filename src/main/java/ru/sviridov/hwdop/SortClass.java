package ru.sviridov.hwdop;

import java.util.Random;

public class SortClass {
    Random rand = new Random();
    private int[] arrayInt;

    public SortClass(int elemts) {
        this.arrayInt = new int[elemts];

        for (int i = 0; i < elemts; i++) {
            this.arrayInt[i] = rand.nextInt(101);
        }
    }

    public int[] getArrayInt() {
        return arrayInt;
    }


    private void swap(int first, int second) {
        int buffer = this.arrayInt[first];
        arrayInt[first] = arrayInt[second];
        arrayInt[second] = buffer;

    }

    public void bubleSort(int[] array) {
        for (int i = array.length - 1; i >= 1; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1])
                    swap(j, j + 1);
            }
        }
    }


    public void quickSort(int[] array, int low, int high) {

        if (array.length == 0)
            return;//завершить выполнение если длина массива равна 0

        if (low >= high)
            return;//завершить выполнение если уже нечего делить


        int middle = low + (high - low) / 2;
        int opora = array[middle];


        int i = low, j = high;
        while (i <= j) {
            while (array[i] < opora) {
                i++;
            }

            while (array[j] > opora) {
                j--;
            }

            if (i <= j) {//меняем местами
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }

        // вызов рекурсии для сортировки левой и правой части
        if (low < j)
            quickSort(array, low, j);

        if (high > i)
            quickSort(array, i, high);
    }
}

