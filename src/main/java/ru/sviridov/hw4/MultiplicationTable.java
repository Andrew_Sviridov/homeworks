package ru.sviridov.hw4;

public class MultiplicationTable {
    public static void main(String[] args) {
        for (int i : new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}) {
            for (int j : new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}) {
                System.out.print("\t" + i * j);
            }
            System.out.println();
        }
    }
}
