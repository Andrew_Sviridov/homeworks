package ru.sviridov.hw4;

import java.util.Scanner;

public class GeometricProgression {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Input first element: ");
        int firstElement = in.nextInt();
        System.out.print("Input denominator: ");
        int denominator = in.nextInt();
        System.out.print("Input N: ");
        int numberN = in.nextInt();
        for (int j = 1; j <= numberN; j++) {
            System.out.print(" " + firstElement * Math.pow(denominator, (j - 1)));
        }
        in.close();

    }
}