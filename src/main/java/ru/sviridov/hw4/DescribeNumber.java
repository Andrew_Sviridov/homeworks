package ru.sviridov.hw4;

import java.util.Scanner;

public class DescribeNumber {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Input a number: ");
        int number = in.nextInt();
        if (number == 0) {
            System.out.println("ноль");
        } else if (number > 0) {
            System.out.println("положительное");
        } else {
            System.out.println("отрицательное");
        }
        if (number % 2 == 0) {
            System.out.println("четное ");
        } else {
            System.out.println("нечетное");
        }
        in.close();
    }
}