package ru.sviridov.hw6;

public class Student extends Person {
    private int studentID;

    public Student(String name, String lastName, int numberPhone) {
        super(name, lastName, numberPhone);
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }
}
