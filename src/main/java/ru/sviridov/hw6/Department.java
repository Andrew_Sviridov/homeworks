package ru.sviridov.hw6;

public class Department {
    private String nameDepartment;
    private Teacher[] Teachers;

    public Department(String nameDepartment, Teacher[] teachers) {
        this.nameDepartment = nameDepartment;
        this.Teachers = teachers;
    }


    public String getNameDepartment() {
        return nameDepartment;
    }

    public void setNameDepartment(String nameDepartment) {
        this.nameDepartment = nameDepartment;
    }

    public Teacher[] getTeachers() {
        return Teachers;
    }

    public void setTeachers(Teacher[] teachers) {
        Teachers = teachers;
    }

    public void addTeacher(Teacher teacher) {

    }

    public void removeTeacher(Teacher teacher) {

    }

}
