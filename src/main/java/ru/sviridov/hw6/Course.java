package ru.sviridov.hw6;

public class Course {
    private String nameCourse;
    private int courseID;

    public Course(String nameCourse, int courseID) {
        this.nameCourse = nameCourse;
        this.courseID = courseID;
    }

    public int getCourseID() {
        return courseID;
    }

    public void setCourseID(int courseID) {
        this.courseID = courseID;
    }

    public String getNameCourse() {
        return nameCourse;
    }

    public void setNameCourse(String nameCourse) {
        this.nameCourse = nameCourse;
    }
}
