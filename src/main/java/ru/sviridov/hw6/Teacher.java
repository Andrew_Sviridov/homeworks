package ru.sviridov.hw6;

public class Teacher extends Person {
    private int teacherID;

    public Teacher(String name, String lastName, int numberPhone, int teacherID) {
        super(name, lastName, numberPhone);
        this.teacherID = teacherID;
    }

    public int getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(int teacherID) {
        this.teacherID = teacherID;
    }
}
