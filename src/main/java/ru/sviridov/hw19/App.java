package ru.sviridov.hw19;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        String path = "src\\main\\java\\ru\\sviridov\\hw19\\";


        try (Scanner scan = new Scanner(new File(path + "products.txt"))) {
            scan.useDelimiter("~|\r\n|\n");
            System.out.printf("%-11s%11s%11s%15s%n", "Наименование", "Цена", "Кол-во", "Стоимость");
            System.out.println("===================================================");

            String stringTitle = "";

            float number1 = 0;
            float itogo = 0;

            while (scan.hasNext()) {

                stringTitle = scan.next();

                if (scan.hasNextInt()) {
                    number1 = scan.nextFloat();
                } else if (scan.hasNextFloat()) {
                    number1 = scan.nextFloat();
                }

                Float number2 = scan.nextFloat();

                itogo = itogo + number2 * number1;

                System.out.println(String.format("%-20s%-5.2f%s%-5.2f%10s%.2f", stringTitle, number2, " X ", number1, "=", number2 * number1));


            }
            System.out.println("===================================================");
            System.out.printf("%-11s%35.2f", "Итого", itogo);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
