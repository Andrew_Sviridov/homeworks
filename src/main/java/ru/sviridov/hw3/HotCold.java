package ru.sviridov.hw3;

import java.util.Random;
import java.util.Scanner;

public class HotCold {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random rand = new Random();
        int previousNumber = 0;
        int rndNumber = rand.nextInt(101);
        System.out.println("Чтобы выйти из игры введите число 999");
        System.out.println("Введите число от 0 до 100");
        while (true) {
            System.out.print("Введите число: ");
            int numberIn = in.nextInt();
            if (numberIn == 999) break;
            if ((0 <= numberIn) && (numberIn <= 100)) {
                if (numberIn == rndNumber) {
                    System.out.println("Вы угадали");
                    break;
                } else if (Math.abs(rndNumber - previousNumber) <= Math.abs(rndNumber - numberIn)) {
                    System.out.println("Холодно");
                } else if (Math.abs(rndNumber - previousNumber) > Math.abs(rndNumber - numberIn)) {
                    System.out.println("Горячо");
                }
                previousNumber = numberIn;
            } else {
                System.out.println("Вы ввели некорректное число.");
            }

        }
        in.close();
    }
}