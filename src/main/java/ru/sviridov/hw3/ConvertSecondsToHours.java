package ru.sviridov.hw3;

import java.util.Scanner;

public class ConvertSecondsToHours {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Input a number: ");
        float seconds = in.nextFloat();
        if (seconds != 0) {
            System.out.println(seconds / 3600);
        }
        in.close();
    }
}
