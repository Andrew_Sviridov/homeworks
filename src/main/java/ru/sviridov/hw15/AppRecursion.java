package ru.sviridov.hw15;

import java.io.File;

public class AppRecursion {
    public static void main(String[] args) {
        String path = "src\\main\\java\\ru\\sviridov\\hw15\\";
        createTestData(path);
        File file1 = new File(path + "testData2");
        showFile2(file1, 1);
        // showFile(file1, 0);
    }


    private static void showFile2(File root, int depth) {
        if (root != null && root.exists()) {
            if (root.isDirectory()) {
                System.out.println(spaceRepeat(depth) + root.getName() + " <DIR>");
                for (File file : root.listFiles()) {

                    showFile2(file, depth + 1);
                }
            } else {
                System.out.println(spaceRepeat(depth) + root.getName() + " " + root.length());
            }
        }

    }

    public static String spaceRepeat(int depth) {
        String s = "";
        for (int i = 0; i < depth; i++) {
            s += " ";
        }
        return s;
    }

// моя реализация. было
//    private static int showFile(File root, int depth) {
//        int result = depth;
//        if (root != null && root.exists()) {
//            if (root.isDirectory()) {
//                for (File file : root.listFiles()) {
//                    if (file.isFile()) {
//
//
//                        System.out.println(Space(result) + file.getName() + " " + file.length());
//                        continue;
//
//
//                    } else if (file.isDirectory()) {
//
//                        System.out.println(Space(result) + file.getName() + " <DIR>");
//                        result++;
//                        result = showFile(file, result);
//                        continue;
//                    }
//                }
//            }
//
//        }
//        return result - 1;
//    }


    private static void createTestData(String path) {
        try {
            new File(path + "testData2/a/b/").mkdirs();
            new File(path + "testData2/a/c/").mkdirs();
            new File(path + "testData2/b/a/").mkdirs();
            new File(path + "testData2/b/c/").mkdirs();
            new File(path + "testData2/a/1.txt").createNewFile();
            new File(path + "testData2/b/2.txt").createNewFile();
            new File(path + "testData2/b/a/3.txt").createNewFile();
            //  new File(path + "testData2/b/a/c/d/4.txt").createNewFile();
            //new File(path + "testData2/a/b/c/d/5.txt").createNewFile();
        } catch (Exception e) {
        }
    }
}