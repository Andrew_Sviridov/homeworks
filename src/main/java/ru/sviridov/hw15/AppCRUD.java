package ru.sviridov.hw15;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class AppCRUD {
    public static void main(String[] args) throws IOException {

        Path path = Paths.get("src\\main\\java\\ru\\sviridov\\hw15\\file1.txt");
        Path pathFrom = Paths.get("src\\main\\java\\ru\\sviridov\\hw15\\newFile.txt");
        Path pathTo = Paths.get("src\\main\\java\\ru\\sviridov\\hw15\\newFileCopy.txt");

        try {
            createFile(path);
            renameFile(path, "newFile.txt");

            copyFile(pathFrom, pathTo);

            deleteFile(pathFrom);
            deleteFile(pathTo);

        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("Исключение " + ex.getMessage());
        }
    }


    public static void createFile(Path path) throws IOException {
        new File(String.valueOf(path)).createNewFile();
    }


    public static void renameFile(Path path, String newName) throws IOException {
        File path_to_file = path.toFile();
        String fileParent = path_to_file.getParent();
        path = Paths.get(fileParent + "\\" + newName);
        path_to_file.renameTo(path.toFile());
    }


    public static void copyFile(Path pathFrom, Path pathTo) throws IOException {
        Files.copy(pathFrom, pathTo, REPLACE_EXISTING);
    }


    public static void deleteFile(Path path) throws IOException {
        File path_to_file = path.toFile();
        path_to_file.delete();
    }

}
