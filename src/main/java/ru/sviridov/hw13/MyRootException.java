package ru.sviridov.hw13;

public class MyRootException extends Exception {
    public MyRootException(String message) {
        super(message);
    }
}