package ru.sviridov.hw13;

import static ru.sviridov.hw13.RaisedChild.eat;

public class Mother {
    public static void main(String[] args) {
        String result = "";
        try {
            result = eat(foodEnum.Porridge);
            System.out.println(result);
        } catch (MyRootException e) {
            System.out.println("Произошла ошибка  " + e.getMessage());
        } finally {
            System.out.println("спасибо, мама");

        }
    }
}
