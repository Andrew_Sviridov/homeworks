package ru.sviridov.hw13;

public class RaisedChild {

    public static String eat(foodEnum food) throws MyRootException {
        String result = null;
        switch (food) {
            case Carrot:
                throw new MyRootException("не любит морковь");
            case Apple:
                throw new MyRootException("не любит яблоко");
            case Porridge:
                result = "съел … за обе щеки";
                break;
        }
        return result;
    }

}
